import React, { useEffect, useState } from "react";

import s from "./UsersList.module.css";

const endpoint = "https://jsonplaceholder.typicode.com";

function UsersList() {
  const [userCard, setUserCard] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`${endpoint}/users`);
      const data = await response.json();
      setUserCard(data);
    };
    fetchData();
  }, []);

  // console.log(userCard);

  return (
    <div className={s.wrapper}>
      {
        userCard.map((user, index) => {
          return (
            <div className={s.userCart} key={index}>
              <p className={s.userUsername}>
                <span className={s.userTitle}>username: </span>
                {user.username}
              </p>
              <p className={s.userName}>
                <span className={s.userTitle}>name: </span>
                {user.name}
              </p>
              <p className={s.userEmail}>
                <span className={s.userTitle}>email: </span>
                {user.email}
              </p>
            </div>
          );
        })
      }
    </div>
  );
}

export default UsersList;