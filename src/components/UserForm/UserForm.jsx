import React, { useState } from "react";

import Button from "../Button"; 

import s from "./UserForm.module.css";

const endpoint = "https://jsonplaceholder.typicode.com"

function UserForm() {
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [description, setDescription] = useState('')

    const [isFetching, setIsFetching] = useState(false)

    const onNameChange = (event) => {
        setName(event.target.value)
    }
    const onSurnameChange = (event) => {
        setSurname(event.target.value)
    }
    const onDescriptionChange = (event) => {
        setDescription(event.target.value)
    }

    const onSubmit = async (event) => {
        event.preventDefault()

        try {
          setIsFetching(true);
          const response = await fetch(`${endpoint}/users`, {
            method: "POST",
            body: JSON.stringify({
              name,
              surname,
            }),
            headers: {
              "Content-Type": "application/json",
            }
          });

          if(!response.ok) throw new Error("Server Error :(")

          const data = await response.json();

          console.log("data:", data);
        } catch(error) {
          console.log("error:", error);
        } finally {
          setIsFetching(false);
        }
    }

  return (
    <form className={s.userForm} onSubmit={onSubmit}>
      <label className={s.label}>
        <span className={s.labelText}>Name</span>
        <input 
        type="text" 
        className={s.input} 
        value={name} 
        onChange={onNameChange}
        />
      </label>
      <label className={s.label}>
        <span className={s.labelText}>Surname</span>
        <input 
        type="text" 
        className={s.input} 
        value={surname} 
        onChange={onSurnameChange}
        />
      </label>
      <label className={s.label}>
        <span className={s.labelText}>Description</span>        
        <textarea 
        className={s.input}
        value={description}
        onChange={onDescriptionChange}
        />
      </label>
      <Button 
      className={s.submit} 
      type="submit"
      disabled={isFetching}
      >
        {isFetching ? "Loading..." : "Submit"} 
      </Button>
    </form>
  );
}

export default UserForm;
