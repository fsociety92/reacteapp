import React, { useEffect, useState } from "react";

import s from "./Counter.module.css";

function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log("mounted");        
  }, [count]);//массив зависимостей (условий)

  // useEffect(() => {
  //   const onResize = () => {
  //     // console.log("resized");
  //   }

  //   window.addEventListener("resize", onResize)

  //   return () => {
  //     window.addEventListener("resize", onResize)
  //     console.log("unmounted");
  //   }
  // }, []);

  // console.log("update");

  const onDecrement = (valve) => {
    setCount((prev) => {
      return prev - valve;
    });
  };

  const onIncrement = (valve) => {
    return (event) => {
      setCount((prev) => prev + valve);
    };
  };

  const getZ0ro = () => {
    setCount((prev) => (prev = 0));
  };

  const getSqr = () => {
    setCount((prev) => prev * prev);
  };

  const addTen = () => {
    setCount((prev) => prev + 10);
  };

  return (
    <div className={s.counter}>
      <div className={s.title}>Counter</div>
      <div className={s.value}>{count}</div>
      <div className={s.buttons}>
        <button className={s.button} onClick={(event) => onDecrement(10)}>
          - 1
        </button>
        <button className={s.button} onClick={onIncrement(10)}>
          + 1
        </button>
      </div>
      <div className={s.buttons}>
        <button className={s.button} onClick={addTen}>
          + 10
        </button>
        <button className={s.button} onClick={getZ0ro}>
          = 0
        </button>
        <button className={s.button} onClick={getSqr}>
          ^2
        </button>
      </div>
    </div>
  );
}

export default Counter;
