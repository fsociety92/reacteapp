import React from "react";

import s from "./ProfileCard.module.css"

function ProfileCard ({profile, theme, linkedin}) {
    return (
        <div className={s.wrapper}>
            {/* <div className={s.imageDiv}> */}
            <img
            className={s.image}
            src={profile.image}
            />
            {/* </div> */}
            <div className={theme ? s.main : s.mainDark}>
                <div className={theme ? s.contentDiv : s.contentDivWhite}>
                    <p className={s.name}>
                        {profile.name + " " + profile.surname}
                    </p>
                    <p className={s.status}>
                        {profile.status}
                    </p>
                    <p className={s.website}>
                        {profile.website}
                    </p>
                    <div className={s.buttonsDiv}>
                        <div className={s.emailBtn}>
                            <img src={profile.buttons.email.icon}/>
                            <span>{profile.buttons.email.name}</span>
                        </div>
                        {
                            linkedin && 
                            <div className={s.linkedinBtn}>
                                <img src={profile.buttons.linkedin.icon}/>
                                <span>{profile.buttons.linkedin.name}</span>
                            </div>
                        }
                    </div>
                    <div className={s.aboutDiv}>
                        <h2>About</h2>
                        <p>{profile.about}</p>
                    </div>
                    <div className={s.interestsDiv}>
                        <h2>Interests</h2>
                        <p>{profile.interests}</p>
                    </div>
                </div>
            </div>
            <div className={theme ? s.footerDiv : s.footerDivDark}>
                <div className={s.footerContent}>
                    {
                        profile.otherLinks.map((elem, index) => {
                            if(elem.name == "LinkedIn") {
                                return (
                                    (!linkedin) &&
                                    <div className={s.iconDiv} key={index}>
                                        <img
                                        className={s.icon}
                                        src={elem.icon}
                                        />
                                    </div>
                                )                            
                            }
                            else {
                                // console.log(elem.name);
                                return (
                                    <div className={s.iconDiv} key={index}>
                                        <img
                                        className={s.icon}
                                        src={elem.icon}
                                        />
                                    </div>
                                )                                
                            }
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default ProfileCard;