import React, { useState } from "react";

import StarIcon from "../../images/StarIcon.svg";
import CheckIcon from "../../images/CheckIcon.svg"

import s from "./AuthorizationForm.module.css";

const endpoint = "https://jsonplaceholder.typicode.com"

function AuthorizationForm() {
  const [switcher, setSwitcher] = useState("auth");
  const authSwitcher = () => setSwitcher((prev) => prev = "auth");
  const regSwitcher = () => setSwitcher((prev) => prev = "reg");

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirm, setConfirm] = useState('');

  const onEmailChange = (event) => setEmail(event.target.value);
  const onPasswordChange = (event) => setPassword(event.target.value);
  const onConfirmChange = (event) => setConfirm(event.target.value);

  const [accept, setAccept] = useState(false);
  const acceptClick = () => setAccept((prev) => !prev);

  const submit = async () => {
    if(!email) throw new Error("incorrect email");
    if(!password) throw new Error("incorrect password");

    if(switcher === "reg") {
      if(!confirm) throw new Error("incorrect confirm");
      if(!accept) throw new Error("do not accepted");
    }

    try {
      const response = await fetch(`${endpoint}/users`, {
        method: "POST",
        body: JSON.stringify({
          email,
          password,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if(!response.ok) throw new Error("Server Error :(");

      const data = await response.json();

      console.log("data:", data);

    } catch (error) {

      console.log("error:", error);

    } finally {
      
      setEmail((prev) => prev = "");
      setPassword((prev) => prev = "");

      if(switcher === "reg") {
        setConfirm((prev) => prev = "");
        setAccept((prev) => !prev);
      }

      alert("Completed!");
    }
  }
  return (
    <div className={s.wrapper}>
      <div className={s.starDiv}>
        <img className={s.star} src={StarIcon} />
      </div>
      <div className={s.switcher}>
        <div 
        className={(switcher == "auth") && s.activeButton}
        onClick={authSwitcher}
        >
          Sign up
        </div>
        <div 
        className={(switcher == "reg") && s.activeButton}
        onClick={regSwitcher}
        >
          Register
        </div>
      </div>
      <div className={s.form}>
        <p className={s.emailText}>Email address</p>
        <input 
        className={s.emailInput} 
        type="text" 
        placeholder="Your email"
        value={email}
        onChange={onEmailChange}
        />
        <p className={s.passwordText}>Password</p>
        <input 
        className={s.passwordInput} 
        type="text" 
        placeholder="Password"
        value={password}
        onChange={onPasswordChange}
        />
        {
          (switcher == "reg") &&
          <>
            <p className={s.confirmText}>Confirm</p>
            <input 
            className={s.confirmInput} 
            type="text" 
            placeholder="Confirm"
            value={confirm}
            onChange={onConfirmChange}
            />
            <div className={s.acceptDiv}>
              <div className={s.checkbox} onClick={acceptClick}>
                {
                  accept &&
                  <img src={CheckIcon} className={s.checkIcon} />
                }
                {/* {
                  accept && 
                  <svg width="60%" viewBox="0 0 10 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 3.18182L4.11111 7L9 1" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                } */}
              </div>
              <span>I accept the terms and privacy policy</span>
            </div>
          </>
        }
        <div 
        className={`${s.formButton} ${(switcher == "auth") ? s.authFormButton : s.regFormButton}`}
        onClick={submit}
        >
          {(switcher == "auth") ? "Sign in" : "Register"}
        </div>
      </div>
    </div>
  );
}

export default AuthorizationForm;