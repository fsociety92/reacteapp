import React from "react";

import s from "./PostShow.module.css";

function PostShow({ title, content }) {
  return (
    <div className={s.postDiv}>
      <h1 className={s.title}>{title}</h1>
      <p className={s.content}>{content}</p>
    </div>
  );
}

export default PostShow;