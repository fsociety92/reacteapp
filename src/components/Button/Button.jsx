import React from "react";

import s from "./Button.module.css"

function Button ({className, children, color, type = "button", disabled = false}) {
    return (
        <button
        className={`${s.button} ${className}`}
        type={type}
        disabled={disabled}
        style={{color: color}}
        >
            {children}
        </button>
    )
}

export default Button;