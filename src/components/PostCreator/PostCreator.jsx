import React, { useState } from "react";

import Button from "../Button";

import s from "./PostCreator.module.css"

function PostCreator ({submitHandler}) {

    const [title, setTitle] = useState("")
    const [content, setContent] = useState("")

    const onTitleChange = (event) => {
        setTitle(event.target.value)
    }
    const onContentChange = (event) => {
        setContent(event.target.value)
    }

    const onSubmit = (event) => {
        event.preventDefault();

        if(!title) alert("incorrect title")
        if(!content) alert("incorrect content")

        console.log(`${title}-${content}`)

        submitHandler({title,content})

        setTitle("");
        setContent("");
    }

    return (
        <div className={s.wrapper}>
            <form className={s.userForm} onSubmit={onSubmit}>
                <label className={s.label}>
                    <span className={s.spanText}>Title</span>
                    <input 
                    type="text" 
                    className={s.input}
                    value={title}
                    onChange={onTitleChange}
                    />
                </label>
                <label className={s.label}>
                    <span className={s.spanText}>Content</span>
                    <textarea
                    className={s.input}
                    value={content}
                    onChange={onContentChange}
                    />
                </label>
                <Button
                className={s.submit}
                type="submit"
                >
                    Submit
                </Button>
            </form>
        </div>
    )
}

export default PostCreator;