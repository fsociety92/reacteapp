import React, { useState } from "react";

import TennisCard from "./components/TennisCard";
import UserForm from "./components/UserForm";
import PostCreator from "./components/PostCreator";
import PostShow from "./components/PostShow";
import Counter from "./components/Counter";
import UsersList from "./components/UsersList";
import ProfileCard from "./components/ProfileCard";
import AuthorizationForm from "./components/AuthorizationForm";
// import Intro from "./components/Intro/Intro";
import { tennisData } from "./data/tennis";
import { profileData } from "./data/profile";

import "./styles.css";

function App() {
  const [postData, setPostData] = useState([]);

  const addPostHandler = (newPost) => {
    setPostData((prev) => [...prev, newPost]);
  };

  const [isCounterShown, setIsCounterShown] = useState(true);

  const showCounter = () => {
    setIsCounterShown((prev) => !prev);
  };

  const buttonText = isCounterShown ? "hide" : "show";

  const [darkTheme, setDarkTheme] = useState(true)

  const themeSwitcher = () => {
    setDarkTheme((prev) => (!prev))
  }

  const [linkedIn, setLinkedIn] = useState(true)

  const linkedInSwitcher = () => {
    setLinkedIn((prev) => (!prev))
  }

  return (
    <div className="main">

      <div className="authorizationFormDiv">
        <AuthorizationForm />
      </div>

      {/* ///////////////////////////////////////////////// */}

      <div>
        <button onClick={themeSwitcher}>
          {darkTheme ? "Light Theme" : "Dark Theme"}
        </button>
        <button onClick={linkedInSwitcher}>
          {linkedIn ? "Without LinkedIn" : "With LinkedIn"}
        </button>
      </div>

      <div className="profileCard">
        {
          profileData.map((elem) => (
            <ProfileCard
            key={elem.id}
            profile={elem}
            theme={darkTheme}
            linkedin={linkedIn}
            />
          ))
        }
      </div>      

      {/* ///////////////////////////////////////////////// */}

      <div className="grid">
        {tennisData.map((elem) => (
          <TennisCard
            key={elem.id}
            image={elem.image}
            date={elem.date}
            text={elem.text}
            buttonText={elem.buttonText}
          />
        ))}
      </div>      

      {/* ///////////////////////////////////////////////// */}
        
        < UsersList />
        <br />

      {/* ///////////////////////////////////////////////// */}

      <PostCreator submitHandler={addPostHandler} />
      <br />
      {
        postData.map((post, index) => (
            <PostShow 
            key={index} 
            title={post.title} 
            content={post.content} 
            />
        ))
      }

      {/* ///////////////////////////////////////////////// */}

      <button type="button" onClick={showCounter}>
        {buttonText}
      </button>

      {isCounterShown && <Counter />}
      <br />

      {/* ///////////////////////////////////////////////// */}

      <UserForm />

      {/* ///////////////////////////////////////////////// */}
    </div>
  );
}

export default App;
