import Profile1 from "../images/Profile1.png"
import GitHubIcon from "../images/GitHubIcon.svg"
import InstagramIcon from "../images/InstagramIcon.svg"
import FacebookIcon from "../images/FacebookIcon.svg"
import TwitterIcon from "../images/TwitterIcon.svg"
import EmailIcon from "../images/EmailIcon.svg"
import LinkedInIconSmall from "../images/LinkedInIcon.svg"
import LinkedInIconLarge from "../images/LinkedInIconLarge.svg"

export const profileData = [
    {
        id: 1,
        image: Profile1,
        name: "Laura",
        surname: "Smith",
        status: "Frontend Developer",
        website: "lauraSmith.website",
        buttons: {
            email: {
                name: "Email",
                icon: EmailIcon,
            },
            linkedin: {
                name: "LinkedIn",
                icon: LinkedInIconSmall,
            },
        },
        about: "I am a frontend developer with a particular interest in making things simple and automating daily tasks. I try to keep up with security and best practices, and am always looking for new things to learn.",
        interests: "Food expert. Music scholar. Reader. Internet fanatic. Bacon buff. Entrepreneur. Travel geek. Pop culture ninja. Coffee fanatic.",
        otherLinks: [
            {
                name: "Twitter",
                icon: TwitterIcon,
            },
            {
                name: "Facebook",
                icon: FacebookIcon,
            },
            {
                name: "Instagram",
                icon: InstagramIcon,
            },
            {
                name: "LinkedIn",
                icon: LinkedInIconLarge,
            },
            {
                name: "GitHub",
                icon: GitHubIcon,
            },
        ],
    },
];